module.exports = {
  name: 'api-init',
  run: async toolbox => {
    const { print: { success, error, info, warning, colors, spin }, system } = toolbox
    let spinner = spin('Time to fun')
    // await toolbox.system.run('sleep 5')
    await system.run('sleep 5')
    spinner.succeed(`I'm happy 😄️`)
    success('Welcome to your CLI')
    error('Welcome to your CLI')
    info('Welcome to your CLI')
    warning('Welcome to your CLI')
    colors.muted('Welcome to your CLI')
    // console.muted('teste')
  }
}
