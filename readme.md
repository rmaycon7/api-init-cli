# API Initial Templete Generator CLI

A CLI for api-init.

## Customizing your CLI



### Requiriments
- NodeJS >=8.x
- pnpm

Installation guide:
- Linux

```shell
sudo pnpm add -g api-init
```
- OsX and Windows

```shell
pnpm add -g api-init
```

<!-- 
## Publishing to NPM -->

<!-- To package your CLI up for NPM, do this:

```shell
$ npm login
$ npm whoami
$ npm lint
$ npm test
(if typescript, run `npm run build` here)
$ npm publish
``` -->
Check out the documentation off gluegun at  https://github.com/infinitered/gluegun/tree/master/docs.

# License

MIT - see LICENSE

